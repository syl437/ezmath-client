import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Restangular} from 'ngx-restangular';
import {FormBuilder, Validators} from '@angular/forms';
import * as moment from 'moment';

@IonicPage()
@Component({
    selector: 'page-schedule',
    templateUrl: 'schedule.html',
})
export class SchedulePage {

    today = moment().format('YYYY-MM-DD');
    subjects: Array<any> = [];
    branches: Array<any> = [];
    lessons: Array<any> = [];
    form = this.fb.group({
        branch: ['', Validators.required],
        subject: ['', Validators.required],
        date: [this.today, Validators.required],
    });

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public restangular: Restangular,
                public fb: FormBuilder,
                public alertCtrl: AlertController) {}

    async ionViewWillEnter () {
        try {
            this.branches = await this.restangular.all('branches').getList().toPromise();
        } catch (err){
            console.log(err);
        }
    }

    async watchBranch () {
        try {
            let response = await this.restangular.one('branches', this.form.value.branch).getList().toPromise();
            for (let item of response){
                this.subjects = this.subjects.concat(item.subjects);
            }
        } catch (err){
            console.log(err);
        }
    }

    async getLessons (){
        if (this.form.invalid){
            return;
        }

        try {
            this.lessons = await this.restangular.all('lessons').customGET('', {subject_id: this.form.value.subject, date: this.form.value.date}).toPromise();
        } catch (err){
            console.log(err);
        }

    }

    async enroll (lesson) {
        try {
            await this.restangular.all('lessons').customPOST({lesson_id: lesson.id}).toPromise();
            this.alertCtrl.create({title: 'Successfully enrolled', buttons: ['OK']}).present();
            this.getLessons();
        } catch (err){
            console.log(err);
        }
    }

    async unenroll (lesson) {
        try {
            await this.restangular.all('lessons').customPOST({lesson_id: lesson.id}, 'detach').toPromise();
            this.alertCtrl.create({title: 'Successfully removed', buttons: ['OK']}).present();
            this.getLessons();
        } catch (err){
            console.log(err);
        }
    }
}
