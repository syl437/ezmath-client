import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FormBuilder, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage {

    // form = this.fb.group({
    //     email: ['test@test.com', [Validators.required, Validators.email]],
    //     password: ['123456', Validators.required],
    //     name: ['Test', Validators.required],
    //     phone: ['0524208555', [Validators.required, Validators.minLength(9)]],
    //     city: ['Test', Validators.required],
    //     grade: ['א', Validators.required],
    //     level: ['א', Validators.required],
    //     school_name: ['Test', Validators.required],
    //     parent_name: ['Test', Validators.required],
    //     parent_phone: ['0524208555', [Validators.required, Validators.minLength(9)]],
    // });
    form = this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required],
        name: ['', Validators.required],
        phone: ['', [Validators.required, Validators.minLength(9)]],
        city: ['', Validators.required],
        grade: ['א', Validators.required],
        level: ['א', Validators.required],
        school_name: ['', Validators.required],
        parent_name: ['', Validators.required],
        parent_phone: ['', [Validators.required, Validators.minLength(9)]],
    });
    grades = ['א', 'ב', 'ג', 'ד', 'ה', 'ן', 'ז', 'ח', 'ט'];
    levels = ['א', 'ב', 'ג', 'ד', 'ה', 'ן', 'ז', 'ח', 'ט'];

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private fb: FormBuilder,
                private restangular: Restangular) {
    }

    async submit() {

        if (!this.form.valid){
            return;
        }

        let payload: any = {};
        payload.email = this.form.value.email;
        payload.password = this.form.value.password;
        payload.name = this.form.value.name;
        payload.phone = this.form.value.phone;
        payload.city = this.form.value.city;
        payload.grade = this.form.value.grade;
        payload.level = this.form.value.level;
        payload.school_name = this.form.value.school_name;
        payload.parent_name = this.form.value.parent_name;
        payload.parent_phone = this.form.value.parent_phone;

        try {
            await this.restangular.all('students').customPOST(payload).toPromise();
            this.navCtrl.setRoot("HomePage");
        } catch (err){
            console.log(err);
        }
    }

}
